const {
	getCircleArea,
	checkIfPassed,
	getAverage,
	getSum,
	getDifference,
	div_check,
	isOddOrEven,
	reverseString
} = require("../src/util.js");
const {expect, assert} = require("chai");

// test case - a condition we are testing
// it(StringexplainsWhatTheTestDoes, function)
// assert is used to assert conditions for the test to pass. If the assertion fails then the test is considered test
// describe() is used to create a test suite. A test suite is a group of test
// cases related to one another or test the same method, data or function

// describe("test_get_area_circle_area", () => {
// 	it("test_area_of_circle_radius_15_is_706.86", () => {
// 		let area = getCircleArea(15);
// 		assert.equal(area, 706.86);
// 	});

// 	it("test_area_of_circle_radius_300_is_282744", () => {
// 		let area = getCircleArea(300);
// 		expect(area).to.equal(282744);
// 	});
// });

// it("test_area_of_circle_radius_15_is_706.86", () => {
// 	let area = getCircleArea(15);
// 	assert.equal(area, 706.86);
// })

// describe("test_check_if_passed", () => {
// 	it("test_25_out_of_30_if_passed", () => {
// 		let isPassed = checkIfPassed(25, 30);
// 		assert.equal(isPassed, true);
// 	});

// 	it("test_30_out_of_50_is_not_passed", () => {
// 		let isPassed = checkIfPassed(30, 50);
// 		assert.equal(isPassed, false);
// 	});
// });

// describe("test_check_if_average", () => {
// 	it("test_average_of_80_82_84_86_is_83", () => {
// 		let isAverage = getAverage(80, 82, 84, 86);
// 		assert.equal(isAverage, 83);
// 	});

// 	it("test_average_of_70_80_82_84_is_79", () => {
// 		let isAverage = getAverage(70, 80, 82, 84);
// 		assert.equal(isAverage, 79);
// 	})
// });

// describe("test_getSum_of_two_number", () => {
// 	it("test_get_sum_of_15_30", () => {
// 		let isSum = getSum(15, 30);
// 		assert.equal(isSum, 45);
// 	})

// 	it("test_get_sum_of_25_50", () => {
// 		let isSum = getSum(25, 50);
// 		assert.equal(isSum, 75);
// 	})
// })

// describe("test_the_difference_of_two_number", () => {
// 	it("test_get_difference_of_70_40", () => {
// 		let isDifference = getDifference(70, 40);
// 		assert.equal(isDifference, 30);
// 	})

// 	it("test_get_difference_of_125_50", () => {
// 		let isDifference = getDifference(125, 50);
// 		assert.equal(isDifference, 75);
// 	})
// })

describe("test_check_divisible_of_a_number", () => {
	it("test_check_if_49_is_divisible_by_7", () => {
		let isDivisible = div_check(49);
		assert.equal(isDivisible, true);
	})

	it("test_check_if_21_is_divisible_by_7", () => {
		let isDivisible = div_check(21);
		assert.equal(isDivisible, true);
	})

	it("test_check_if_25_is_divisible_by_5", () => {
		let isDivisible = div_check(25);
		assert.equal(isDivisible, true)
	});

	it("test_check_if_100_is_divisible_by_5", () => {
		let isDivisible = div_check(100);
		assert.equal(isDivisible, true)
	});
});

describe("test_check_if_number_is_odd_or_even", () => {
	it("test_check_if_18_is_even", () => {
		let isEven = isOddOrEven(18);
		assert.equal(isEven, "even");
	});

	it("test_check_if_26_is_even", () => {
		let isEven = isOddOrEven(26);
		assert.equal(isEven, "even");
	});

	it("test_check_if_1_is_odd", () => {
		let isOdd = isOddOrEven(1);
		assert.equal(isOdd, "odd");
	});

	it("test_check_if_17_is_odd", () => {
		let isOdd = isOddOrEven(17);
		assert.equal(isOdd, "odd");
	});
});

describe("test_check_if_string_is_reverse", () => {
	it("test_check_if_revese_of_hello_is_olleh", () => {
		let isReverse = reverseString("hello");
		assert.equal(isReverse, "olleh");
	});

	it("test_check_if_revese_of_wow_is_wow", () => {
		let isReverse = reverseString("wow");
		assert.equal(isReverse, "wow");
	});

	it("test_check_if_revese_of_freecodecamp_is_pmacedoceerf", () => {
		let isReverse = reverseString("freecodecamp");
		assert.equal(isReverse, "pmacedoceerf");
	});

	it("test_check_if_revese_of_zuitt_is_ttiuz", () => {
		let isReverse = reverseString("zuitt");
		assert.equal(isReverse, "ttiuz");
	});
})