// Sample functions to Test

function getCircleArea(radius) {
	// area = pi * radius(2)
	return 3.1416 * (radius**2);
}

function checkIfPassed(score, total) {
	// score/total*100 > 75
	return (score/total) * 100 >= 75;
}

function getAverage(num1, num2, num3, num4) {
	return (num1+num2+num3+num4)/4
}

function getSum(num1, num2) {
	return num1 + num2;
}

function getDifference(num1, num2) {
	return num1 - num2;
}

function div_check (num1) {
	return num1 % 5 === 0 ? true : (num1 % 7 === 0 ? true : false);
}

function isOddOrEven (num) {
	return num % 2 === 0 ? "even" : "odd";
}

function reverseString(str) {
	return str.split("").reverse().join("");
}

module.exports = {
	getCircleArea: getCircleArea,
	checkIfPassed: checkIfPassed,
	getAverage: getAverage,
	getSum: getSum,
	getDifference: getDifference,
	div_check: div_check,
	isOddOrEven : isOddOrEven,
	reverseString: reverseString
}